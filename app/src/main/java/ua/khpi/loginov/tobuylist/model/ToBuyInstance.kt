package ua.khpi.loginov.tobuylist.model

import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "to_buy_instance")
data class ToBuyInstance(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "text") val text: String?,
    @ColumnInfo(name = "picture_uri") val pictureUri: String?,
    @ColumnInfo(name = "is_bought") var isBought: Boolean) {

    fun getUri(): Uri{
        return Uri.parse(pictureUri)
    }

    override fun equals(other: Any?): Boolean {
        if(other === this)
            return true
        if(other === null)
            return false
        if(other is ToBuyInstance)
            if(other.id == id)
                return true
        return false
    }
}