package ua.khpi.loginov.tobuylist.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import ua.khpi.loginov.tobuylist.R

class ToBuyInstanceAdapter(
    c: Context,
    private val resource: Int,
    var items: ArrayList<ToBuyInstance>
): ArrayAdapter<ToBuyInstance>(c, resource, items){

    private val inflater = LayoutInflater.from(c)

    val map = HashMap<Int, View>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View{
        val view: View
        val holder: ViewHolder

        if(!map.containsKey(position)){
            view = inflater.inflate(resource, parent, false)
            holder = ViewHolder(view)
            view.tag = holder
            view.setTag(R.integer.additional_tag_key, items[position])
            map.put(position, view)

            val item = items[position]

            if(item.text !== null) {
                if(holder.image.visibility == View.VISIBLE)
                    holder.image.visibility = View.GONE

                holder.text.text = item.text
                holder.text.visibility = View.VISIBLE
            } else {
                if(holder.text.visibility == View.VISIBLE)
                    holder.text.visibility = View.GONE

                holder.image.setImageURI(item.getUri())
                holder.image.visibility = View.VISIBLE
            }
            holder.id.text = item.id.toString()
        } else{
            view = map[position] as View
        }

        return  view
    }

    override fun clear() {
        super.clear()
        map.clear()
    }

    class ViewHolder(view: View){
        val id: TextView = view.findViewById(R.id.itemId)
        val text: TextView = view.findViewById(R.id.toBuyText)
        val image: ImageView = view.findViewById(R.id.toBuyImage)
    }
}