package ua.khpi.loginov.tobuylist.di.modules

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ua.khpi.loginov.tobuylist.di.annotations.ApplicationContext
import ua.khpi.loginov.tobuylist.di.annotations.DatabaseName
import ua.khpi.loginov.tobuylist.model.MyDatabase
import ua.khpi.loginov.tobuylist.model.ToBuyInstanceDao
import javax.inject.Singleton

@Module
class MyDatabaseModule(@ApplicationContext private val context: Context) {

    companion object {
        @DatabaseName
        private const val databaseName = "to_nuy_list_db"
    }

    @Provides
    @Singleton
    fun provideDatabase(): MyDatabase {
        return Room.databaseBuilder(
            context,
            MyDatabase::class.java,
            databaseName
        ).build()
    }

    @Provides
    @DatabaseName
    fun getDatabaseName(): String{
        return databaseName
    }

    @Provides
    @Singleton
    fun provideToBuyInstanceDao(db: MyDatabase): ToBuyInstanceDao {
        return db.toBuyInstanceDao()
    }

}